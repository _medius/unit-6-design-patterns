package com.epam.restaurant;

import com.epam.restaurant.orders.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Waiter {

	private List<Order> orders = new ArrayList<>();

	public void setOrder(Order order) {
		orders.add(order);
	}

	public List<Food> ordersUp() {
		final List<Food> food = orders.stream().map(Order::execute).collect(Collectors.toList());
		orders.clear();
		return food;
	}
}
