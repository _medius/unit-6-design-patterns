package com.epam.restaurant.orders;

import com.epam.restaurant.Food;

public interface Order {
	Food execute();
}
