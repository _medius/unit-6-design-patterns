package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.kitchen.PizzaProducer;
import com.epam.restaurant.kitchen.pizzas.ChickenRanch;

public class GimmeChickenRanch implements Order {

	private PizzaProducer producer;

	public GimmeChickenRanch(PizzaProducer producer) {
		this.producer = producer;
	}

	@Override
	public Food execute() {
		return producer.getPizza(ChickenRanch.class);
	}

	public void setProducer(PizzaProducer producer) {
		this.producer = producer;
	}
}
