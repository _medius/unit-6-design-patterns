package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.bar.DrinkProducer;
import com.epam.restaurant.bar.drinks.PinaColada;

public class GimmePinaColada implements Order {

	private DrinkProducer producer;

	public GimmePinaColada(DrinkProducer producer) {
		this.producer = producer;
	}

	@Override
	public Food execute() {
		return producer.getDrink(PinaColada.class);
	}

	public void setProducer(DrinkProducer producer) {
		this.producer = producer;
	}
}
