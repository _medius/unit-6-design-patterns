package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.kitchen.PizzaProducer;
import com.epam.restaurant.kitchen.pizzas.Margarita;

public class GimmeMargarita implements Order {

	private PizzaProducer producer;

	public GimmeMargarita(PizzaProducer producer) {
		this.producer = producer;
	}

	@Override
	public Food execute() {
		return producer.getPizza(Margarita.class);
	}

	public void setProducer(PizzaProducer producer) {
		this.producer = producer;
	}
}
