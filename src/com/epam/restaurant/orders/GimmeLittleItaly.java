package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.kitchen.PizzaProducer;
import com.epam.restaurant.kitchen.pizzas.LittleItaly;

public class GimmeLittleItaly implements Order {

	private PizzaProducer producer;

	public GimmeLittleItaly(PizzaProducer pizzaProducer) {
		this.producer = pizzaProducer;
	}

	@Override
	public Food execute() {
		return producer.getPizza(LittleItaly.class);
	}

	public void setProducer(PizzaProducer producer) {
		this.producer = producer;
	}
}
