package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.bar.DrinkProducer;
import com.epam.restaurant.bar.drinks.Mojito;

public class GimmeMojito implements Order {

	private DrinkProducer producer;

	public GimmeMojito(DrinkProducer producer) {
		this.producer = producer;
	}

	@Override
	public Food execute() {
		return producer.getDrink(Mojito.class);
	}

	public void setProducer(DrinkProducer producer) {
		this.producer = producer;
	}
}
