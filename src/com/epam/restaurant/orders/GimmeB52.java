package com.epam.restaurant.orders;

import com.epam.restaurant.Food;
import com.epam.restaurant.bar.DrinkProducer;
import com.epam.restaurant.bar.drinks.B52;

public class GimmeB52 implements Order {

	private DrinkProducer producer;

	public GimmeB52(DrinkProducer producer) {
		this.producer = producer;
	}

	@Override
	public Food execute() {
		return producer.getDrink(B52.class);
	}

	public void setProducer(DrinkProducer producer) {
		this.producer = producer;
	}
}
