package com.epam.restaurant.bar.drinks;

public final class PinaColada implements Drink  {

	private static final String HOW_TO_DRINK_IT = "Pineapple, coconut and rum what you need!";

	@Override
	public void yumYumDrink() {
		System.out.println(HOW_TO_DRINK_IT);
	}
}
