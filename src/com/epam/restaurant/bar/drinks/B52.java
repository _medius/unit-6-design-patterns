package com.epam.restaurant.bar.drinks;

public final class B52 implements Drink {

	private static final String HOW_TO_DRINK_IT = "Fiery and dangerous B-52, be careful!";

	@Override
	public void yumYumDrink() {
		System.out.println(HOW_TO_DRINK_IT);
	}
}
