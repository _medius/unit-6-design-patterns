package com.epam.restaurant.bar.drinks;

import com.epam.restaurant.Food;

public interface Drink extends Food {
	void yumYumDrink();

	@Override
	default void eatIt() {
		yumYumDrink();
	}
}
