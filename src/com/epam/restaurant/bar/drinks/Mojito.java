package com.epam.restaurant.bar.drinks;

public final class Mojito implements Drink {

	private static final String HOW_TO_DRINK_IT = "Cold, minty mojito, yummy!";

	@Override
	public void yumYumDrink() {
		System.out.println(HOW_TO_DRINK_IT);
	}
}
