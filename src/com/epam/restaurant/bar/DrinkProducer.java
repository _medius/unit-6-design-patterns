package com.epam.restaurant.bar;

import com.epam.restaurant.bar.drinks.Drink;

public interface DrinkProducer {
	Drink getDrink(Class<? extends Drink> drinkType);
}
