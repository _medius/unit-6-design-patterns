package com.epam.restaurant.bar;

import com.epam.restaurant.bar.drinks.Drink;

public class Bar implements DrinkProducer {

	private DrinkProducer drinkShop = new DrinkShop();
	private Barmen barmen;

	public Bar() {
	}

	public Bar(Barmen barmen) {
		this.barmen = barmen;
	}

	public Drink makeDrink(Class<? extends Drink> drinkType) {
		if (barmen == null) {
			System.out.println("Bar have no barmen.");
			return drinkShop.getDrink(drinkType);
		}

		return barmen.cookDrink(drinkType);
	}

	@Override
	public Drink getDrink(Class<? extends Drink> drinkType) {
		return makeDrink(drinkType);
	}
}
