package com.epam.restaurant.bar;

import com.epam.restaurant.bar.drinks.*;

public class Barmen {

	public Drink cookDrink(Class<? extends Drink> drinkType) {
		Drink newDrink = null;
		try {
			newDrink = drinkType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println("Cocktail made by barmen.");
		return newDrink;
	}
}
