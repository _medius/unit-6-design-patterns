package com.epam.restaurant.bar;

import com.epam.restaurant.bar.drinks.*;

public class DrinkShop implements DrinkProducer {

	public Drink sellDrink(Class<? extends Drink> drinkType) {
		Drink newDrink = null;
		try {
			newDrink = drinkType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println("Cocktail bought in drink shop.");
		return newDrink;
	}

	@Override
	public Drink getDrink(Class<? extends Drink> drinkType) {
		return sellDrink(drinkType);
	}
}
