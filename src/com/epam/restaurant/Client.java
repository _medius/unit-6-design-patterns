package com.epam.restaurant;

import com.epam.restaurant.bar.*;
import com.epam.restaurant.kitchen.*;
import com.epam.restaurant.orders.*;

public class Client {

	private Waiter waiter = new Waiter();

	public void makeAnOrderOne() {
		System.out.println("\nClient makes an order in restaurant without cook stuff.\n");
	    PizzaProducer kitchen = new Kitchen();
	    DrinkProducer bar = new Bar();

		GimmeMojito gimmeMojito = new GimmeMojito(bar);
		GimmeLittleItaly gimmeLittleItalyPizza = new GimmeLittleItaly(kitchen);

		waiter.setOrder(gimmeLittleItalyPizza);
		waiter.setOrder(gimmeMojito);

		waiter.ordersUp().forEach(Food::eatIt);
	}

	public void makeAnOrderTwo() {
		System.out.println("\nClient makes an order in restaurant with cook stuff.\n");
		PizzaProducer kitchen = new Kitchen(new PizzaMaker());
		DrinkProducer bar = new Bar(new Barmen());

		GimmeB52 gimmeB52 = new GimmeB52(bar);
		GimmeMargarita gimmeMargarita = new GimmeMargarita(kitchen);
		GimmeChickenRanch gimmeChickenRanch = new GimmeChickenRanch(kitchen);

		waiter.setOrder(gimmeB52);
		waiter.setOrder(gimmeMargarita);
		waiter.setOrder(gimmeChickenRanch);

		waiter.ordersUp().forEach(Food::eatIt);
	}
}
