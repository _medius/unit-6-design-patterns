package com.epam.restaurant.kitchen.pizzas;

public final class LittleItaly implements Pizza {

	private static final String HOW_TO_EAT_IT = "Pizza Little Italy, yummy!";

	@Override
	public void yumYumPizza() {
		System.out.println(HOW_TO_EAT_IT);
	}
}
