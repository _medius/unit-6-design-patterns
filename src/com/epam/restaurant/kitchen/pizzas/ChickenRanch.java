package com.epam.restaurant.kitchen.pizzas;

public final class ChickenRanch implements Pizza {

	private static final String HOW_TO_EAT_IT = "If you are hungry Chicken Ranch is what you need!";

	@Override
	public void yumYumPizza() {
		System.out.println(HOW_TO_EAT_IT);
	}
}
