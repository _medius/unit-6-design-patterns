package com.epam.restaurant.kitchen.pizzas;

import com.epam.restaurant.Food;

public interface Pizza extends Food {
	void yumYumPizza();

	@Override
	default void eatIt() {
		yumYumPizza();
	}
}
