package com.epam.restaurant.kitchen.pizzas;

public final class Margarita implements Pizza {

	private static final String HOW_TO_EAT_IT = "Vegetarian simple but yummy pizza Margarita";

	@Override
	public void yumYumPizza() {
		System.out.println(HOW_TO_EAT_IT);
	}
}
