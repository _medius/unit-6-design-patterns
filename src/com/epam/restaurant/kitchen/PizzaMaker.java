package com.epam.restaurant.kitchen;

import com.epam.restaurant.kitchen.pizzas.*;

public class PizzaMaker {

	public Pizza cookPizza(Class<? extends Pizza> pizzaType) {
		Pizza newPizza = null;
		try {
			newPizza = pizzaType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println("Pizza cooked by pizza maker.");
		return newPizza;
	}
}
