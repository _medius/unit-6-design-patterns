package com.epam.restaurant.kitchen;

import com.epam.restaurant.kitchen.pizzas.Pizza;

public class Kitchen implements PizzaProducer {

	private PizzaProducer pizzaShop = new PizzaShop();
	private PizzaMaker pizzaMaker;

	public Kitchen() {
	}

	public Kitchen(PizzaMaker pizzaMaker) {
		this.pizzaMaker = pizzaMaker;
	}

	public Pizza makePizza(Class<? extends Pizza> pizzaType) {
		if (pizzaMaker == null) {
			System.out.println("Restaurant have no pizza maker.");
			return pizzaShop.getPizza(pizzaType);
		}

		return pizzaMaker.cookPizza(pizzaType);
	}

	@Override
	public Pizza getPizza(Class<? extends Pizza> pizzaType) {
		return makePizza(pizzaType);
	}
}
