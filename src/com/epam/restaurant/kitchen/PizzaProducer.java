package com.epam.restaurant.kitchen;

import com.epam.restaurant.kitchen.pizzas.Pizza;

public interface PizzaProducer {
	Pizza getPizza(Class<? extends Pizza> pizzaType);
}
