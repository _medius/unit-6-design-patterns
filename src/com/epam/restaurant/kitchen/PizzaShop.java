package com.epam.restaurant.kitchen;

import com.epam.restaurant.kitchen.pizzas.*;

public class PizzaShop implements PizzaProducer {

	public Pizza sellPizza(Class<? extends Pizza> pizzaType) {
		System.out.println("Pizza bought in pizza shop.");
		Pizza newPizza = null;
		try {
			newPizza = pizzaType.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return newPizza;
	}

	@Override
	public Pizza getPizza(Class<? extends Pizza> pizzaType) {
		return sellPizza(pizzaType);
	}
}
