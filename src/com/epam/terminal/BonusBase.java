package com.epam.terminal;

import com.epam.terminal.payment.options.PayWithBonuses;

import java.util.HashMap;
import java.util.Map;

public class BonusBase {

	private Map<Integer, PayWithBonuses.BonusMemento> bonusBase = new HashMap<>();

	public void putMemento(Integer phoneNumber, PayWithBonuses.BonusMemento memento) {
		bonusBase.put(phoneNumber, memento);
	}

	public PayWithBonuses.BonusMemento getMemento(Integer phoneNumber) {
		return bonusBase.get(phoneNumber);
	}
}
