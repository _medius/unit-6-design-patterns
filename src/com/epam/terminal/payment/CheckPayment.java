package com.epam.terminal.payment;

public class CheckPayment implements PaymentCalculator {

	@Override
	public int calculatePayment(int orderPrice, int deposit, int phoneNumber) {
		if (orderPrice > deposit) {
			System.out.println("Insufficient funds.");
			return 0;
		}
		return orderPrice;
	}
}
