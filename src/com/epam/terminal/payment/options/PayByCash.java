package com.epam.terminal.payment.options;

import com.epam.terminal.payment.PaymentCalculator;

public class PayByCash extends PayOption {

	public PayByCash(PaymentCalculator payment) {
		super(payment);
	}

	@Override
	public int calculatePayment(int orderPrice, int deposit, int phoneNumber) {
		int paidAmount = payment.calculatePayment(orderPrice, deposit, phoneNumber);

		int changeAmount = deposit - paidAmount;
		System.out.println("Payment by cash. Here is your change " + changeAmount + '.');

		return paidAmount;
	}
}
