package com.epam.terminal.payment.options.selectors;

public enum PaymentMethod {
	PAY_BY_CASH,
	PAY_BY_CARD;
}
