package com.epam.terminal.payment.options;

import com.epam.terminal.payment.PaymentCalculator;

public abstract class PayOption implements PaymentCalculator {

	protected final PaymentCalculator payment;

	public PayOption(PaymentCalculator payment) {
		this.payment = payment;
	}

	@Override
	public int calculatePayment(int orderPrice, int deposit, int phoneNumber) {
		return payment.calculatePayment(orderPrice, deposit, phoneNumber);
	}
}
