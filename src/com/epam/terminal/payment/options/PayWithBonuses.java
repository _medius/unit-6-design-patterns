package com.epam.terminal.payment.options;

import com.epam.terminal.BonusBase;
import com.epam.terminal.payment.PaymentCalculator;

public class PayWithBonuses extends PayOption {

	private static final BonusBase BONUS_BASE = new BonusBase();

	private int bonusPoints;

	public PayWithBonuses(PaymentCalculator payment) {
		super(payment);
	}

	@Override
	public int calculatePayment(int orderPrice, int deposit, int phoneNumber) {
		int paidAmount = payment.calculatePayment(orderPrice, deposit, phoneNumber);

		restoreBonusState(phoneNumber);

		System.out.println(bonusPoints + " bonus points used to pay.");
		paidAmount = orderPrice - bonusPoints;

		int pointsToAdd = paidAmount/10;
		bonusPoints += pointsToAdd;
		System.out.println(pointsToAdd + " bonus points added.");

		saveBonusState(phoneNumber);

		return paidAmount;
	}

	protected void restoreBonusState(int phoneNumber) {
		BonusMemento bonusMemento = BONUS_BASE.getMemento(phoneNumber);
		if (bonusMemento == null) {
			this.bonusPoints = 0;
		} else {
			this.bonusPoints = bonusMemento.getBonusPoints();
		}
	}

	protected void saveBonusState(int phoneNumber) {
		BONUS_BASE.putMemento(phoneNumber, new BonusMemento(bonusPoints));
	}


	public final class BonusMemento {
		private final int bonusPoints;

		private BonusMemento(int bonusPoints) {
			this.bonusPoints = bonusPoints;
		}

		private int getBonusPoints() {
			return bonusPoints;
		}
	}
}
