package com.epam.terminal.payment.options;

import com.epam.terminal.payment.PaymentCalculator;

public class PayByCard extends PayOption {

	public PayByCard(PaymentCalculator payment) {
		super(payment);
	}

	@Override
	public int calculatePayment(int orderPrice, int deposit, int phoneNumber) {
		int paidAmount = payment.calculatePayment(orderPrice, deposit, phoneNumber);
		System.out.println("Payment by card.");

		return paidAmount;
	}
}
