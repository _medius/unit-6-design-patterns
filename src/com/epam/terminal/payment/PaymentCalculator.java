package com.epam.terminal.payment;

public interface PaymentCalculator {

	int calculatePayment(int orderPrice, int deposit, int phoneNumber);
}
