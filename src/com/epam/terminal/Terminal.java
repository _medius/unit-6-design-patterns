package com.epam.terminal;

import com.epam.terminal.payment.*;
import com.epam.terminal.payment.options.*;
import com.epam.terminal.payment.options.selectors.*;

import java.util.*;

public class Terminal {

	private PaymentCalculator calculator = new CheckPayment();

	private PaymentMethod paymentMethod;
	private Set<PaymentOption> paymentOptions = new HashSet<>();

	public void makePayment(Integer orderPrice, Integer deposit, Integer phoneNumber) {
		System.out.println("New payment: orderPrice " + orderPrice + ", deposit " + deposit + '.');
		initOptions();

		int paidAmount = calculator.calculatePayment(orderPrice, deposit, phoneNumber);
		System.out.println("Payment amount " + paidAmount + ".\n");

		calculator = new CheckPayment();
		paymentOptions.clear();
	}

	public void choosePaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void chooseOption(PaymentOption paymentOption) {
		paymentOptions.add(paymentOption);
	}

	private void initOptions() {
		if (paymentOptions.contains(PaymentOption.USE_BONUS_PROGRAM)) {
			calculator = new PayWithBonuses(calculator);
		}

		switch (paymentMethod) {
			case PAY_BY_CASH:   calculator = new PayByCash(calculator);
								break;

			case PAY_BY_CARD:   calculator = new PayByCard(calculator);
								break;
		}
	}
}
