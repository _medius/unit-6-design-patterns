package com.epam.terminal;

import com.epam.terminal.payment.options.selectors.PaymentMethod;
import com.epam.terminal.payment.options.selectors.PaymentOption;

public class MainForTerminal {

	private Terminal terminal = new Terminal();

	private void firstClientPays() {
		System.out.println("\nClient with phone number 123 pays.");

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CASH);
		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(2500, 3000, 123);

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CASH);
		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(1500, 2000, 123);

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CARD);
		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(750, 10_000, 123);
	}

	private void secondClientPays() {
		System.out.println("\nClient with phone number 456 pays.");

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CARD);
		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(1750,10_000, 456);

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CASH);
//		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(1150, 1500, 456);

		terminal.choosePaymentMethod(PaymentMethod.PAY_BY_CASH);
		terminal.chooseOption(PaymentOption.USE_BONUS_PROGRAM);
		terminal.makePayment(1700, 2000, 456);
	}

	private void appStart() {
		firstClientPays();
		secondClientPays();
	}

	public static void main(String[] args) {
		new MainForTerminal().appStart();
	}
}
