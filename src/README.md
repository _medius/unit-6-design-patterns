# Design patterns Homework

## Task 1 pizza restaurant
This task in package com.epam.restaurant
In this task I used pattern Command to make an order by client to 
restaurant, and pattern Chain of responsibility in kitchen and bar 
working logic.

## Task 2 time to pay
This task in package com.epam.terminal
In this task I used pattern Memento to save bonus state of client by
phone number and restore it, and Decorator to add payment options and
choose payment method. We can enable and disable any options and add new
if it needs.